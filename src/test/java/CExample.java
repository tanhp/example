import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class CExample {
    WebDriver driver;

    @Test
    public void MExample(){
        try {
            String command = "chmod +x chromedriver";
            System.out.println("Starting to run command prompt" + command);
            Process p = Runtime.getRuntime().exec(command);

            System.out.println("ABCD 5667657567");
            System.setProperty("webdriver.chrome.driver","chromedriver");
            Map<String, Object> prefs = new HashMap<String, Object>();
            prefs.put("profile.default_content_setting_values.notifications", 2);
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--disable-extensions");
            options.addArguments("--headless");
            options.addArguments("--start-maximized");
            options.addArguments("disable-infobars"); // disabling infobars
            options.addArguments("--disable-extensions"); // disabling extensions
            options.addArguments("--disable-gpu"); // applicable to windows os only
            options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
            options.addArguments("--no-sandbox"); // Bypass OS security model
            options.setExperimentalOption("prefs", prefs);
            options.setExperimentalOption("useAutomationExtension", false);
            driver = new ChromeDriver(options);

            System.out.println("Go to sendo.vn");
            driver.get("https://sendo.vn");
            System.out.println("Get OK");
            driver.close();
            System.out.println("Done!");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
